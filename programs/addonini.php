;<?php/*

[general]
name="icons_awesome"
version="1.0.3"
encoding="UTF-8"
description="An icon theme for ovidentia based on the Font Awesome ( Font Awesome by Dave Gandy - http://fontawesome.io )"
description.fr="Un thème d'icônes pour ovidentia basé sur Font Awesome ( Font Awesome by Dave Gandy - http://fontawesome.io )"
delete=1
ov_version="6.7.5"
php_version="5.1.0"
addon_access_control="0"
mysql_character_set_database="latin1,utf8"
author="Laurent Choulette (laurent.choulette@cantico.fr) (Font Awesome by Dave Gandy - http://fontawesome.io)"
icon="fontawesome.png"
configuration_page="preview"

;*/?>