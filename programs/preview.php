<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

/* @var Func_Icons */
$I = Bab_Functionality::get('Icons/Awesome');

$I->includeCss();

$iconReflector = new ReflectionClass('Func_Icons');

$constants = $iconReflector->getConstants();


$babBody->title = 'Icons samples';

$babBody->babecho('<table align="center" style="border-collapse: collapse;" cellpadding="8"><tbody>');
$babBody->babecho('<tr style="text-align: center;  background-color: #ddd; border: 1px solid #ccc">');
$babBody->babecho('<td>48px</td>');
$babBody->babecho('<td>32px</td>');
$babBody->babecho('<td>24px</td>');
$babBody->babecho('<td>16px</td>');
$babBody->babecho('<td>Symbolic</td>');
$babBody->babecho('<td>48px</td>');
$babBody->babecho('<td>32px</td>');
$babBody->babecho('<td>24px</td>');
$babBody->babecho('<td>16px</td>');
$babBody->babecho('<td>Symbolic</td>');
$babBody->babecho('<td style="border: 1px solid #ccc">CSS classname<br />PHP constant</td>');
$babBody->babecho('</tr>');


$prefixes = array('ACTIONS' => 'ACTIONS', 'APPS' => 'APPS', 'MIMETYPES' => 'MIMETYPES', 'OBJECTS' => 'OBJECTS', 'PLACES' => 'PLACES', 'CATEGORIES' => 'CATEGORIES', 'STATUS' => 'STATUS');

function getPrefix($constantName) {
	global $prefixes;
	list($prefix) = explode('_', $constantName);
	if (array_key_exists($prefix, $prefixes)) {
		return $prefix;
	}
	return false;
}


$previousType = '';
$babBody->addStyleSheet('toolbar.css');
$babBody->babecho('<div class="bab_toolbar">');
foreach($prefixes as $prefix) {
	$babBody->babecho('<a href="#' . $prefix . '">' . $prefix . '</a>&nbsp;');
}
$babBody->babecho('</div>');
foreach($constants as $constantName => $constantValue) {
	if ($prefix = getPrefix($constantName)) {
		if ($prefix != $previousType) {
			$babBody->babecho('<tr id="' . $prefix . '" style="border: 1px solid #ccc; background-color: #ccc; color: #fff"><td colspan="10">' . $prefix . '</td></tr>');
			$previousType = $prefix;
		}
		$babBody->babecho('<tr style="border: 1px solid #ccc">');
		$babBody->babecho('<td class="icon-top-48 icon-48x48 icon-top"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-32 icon-32x32 icon-top"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-24 icon-24x24 icon-top"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-16 icon-16x16 icon-top"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-symbolic icon-symbolic icon-top"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-48 icon-48x48 icon-top" style="background-color: #555; color: #fff"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-32 icon-32x32 icon-top" style="background-color: #555; color: #fff"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-24 icon-24x24 icon-top" style="background-color: #555; color: #fff"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-16 icon-16x16 icon-top" style="background-color: #555; color: #fff"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td class="icon-top-symbolic icon-symbolic icon-top" style="background-color: #555; color: #fff"><span class="icon ' . $constantValue . '"></span></td>');
		$babBody->babecho('<td style="border: 1px solid #ccc">' . $constantValue . '<br />Func_Icons::' . $constantName . '</td>');
		$babBody->babecho('</tr>');
	}
}
$babBody->babecho('</tbody></table>');

/* @var $babBody babBody */
$babBody->babpopup('');


exit;

